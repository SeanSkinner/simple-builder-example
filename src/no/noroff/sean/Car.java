package no.noroff.sean;

public class Car {
    private Engine engine;
    private Tires tires;
    private boolean hasCruiseControl;
    private int cruiseControlSpeed;

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void setTires(Tires tires) {
        this.tires = tires;
    }

    public void setCruiseControlSpeed(int cruiseControlSpeed) {
        this.cruiseControlSpeed = cruiseControlSpeed;
        //return this;
    }

    public void setHasCruiseControl(boolean hasCruiseControl) {
        this.hasCruiseControl = hasCruiseControl;
    }

    public void refuel(String fuel, int amount) throws Exception {
        engine.useFuel(fuel, amount);
    }

    public void changeTires() {
        tires.change();
    }
}
