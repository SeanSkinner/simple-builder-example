package no.noroff.sean;

public class PetrolCarBuilder extends CarBuilder {
    @Override
    public CarBuilder engine() {
        this.engine = new PetrolEngine();
        return this;
    }

    @Override
    public CarBuilder tires(Tires tires) {
        this.tires = tires;
        return this;
    }

    @Override
    public CarBuilder hasCruiseControl(boolean hasCruiseControl) {
        this.hasCruiseControl = hasCruiseControl;
        return this;
    }
}
