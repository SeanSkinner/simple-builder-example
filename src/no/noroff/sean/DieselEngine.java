package no.noroff.sean;

public class DieselEngine implements Engine {
    @Override
    public void useFuel(String fuelType, int amount) throws Exception {
        if (!fuelType.equalsIgnoreCase("diesel")) {
            throw new Exception("Car engine exploded!");
        }

        System.out.println("Car is being refueled with " + amount + "l of diesel");
    }
}
