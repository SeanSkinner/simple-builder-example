package no.noroff.sean;

public class Main {

    public static void main(String[] args) {
	    CarBuilder carBuilder = new PetrolCarBuilder();
        var petrolCar = carBuilder.engine().tires(new RacingTires()).hasCruiseControl(true).build();

        try {
            petrolCar.refuel("petrol", 50);
        } catch (Exception e) {
            e.printStackTrace();
        }

        petrolCar.changeTires();

    }
}
