package no.noroff.sean;

public class PetrolEngine implements Engine {
    @Override
    public void useFuel(String fuelType, int amount) throws Exception {
        if (!fuelType.equalsIgnoreCase("petrol")) {
            throw new Exception("Car engine exploded!");
        }

        System.out.println("Car is being refueled with " + amount + "l of petrol");
    }
}
