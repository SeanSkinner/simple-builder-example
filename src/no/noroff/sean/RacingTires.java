package no.noroff.sean;

public class RacingTires implements Tires {
    @Override
    public void change() {
        System.out.println("A brand new set of racing tires are attached to the car");
    }
}
