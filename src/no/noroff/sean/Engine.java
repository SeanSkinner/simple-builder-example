package no.noroff.sean;

public interface Engine {
    void useFuel(String fuelType, int amount) throws Exception;
}
