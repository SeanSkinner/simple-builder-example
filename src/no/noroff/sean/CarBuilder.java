package no.noroff.sean;

public abstract class CarBuilder {
    protected Engine engine;
    protected Tires tires;
    protected boolean hasCruiseControl;

    public abstract CarBuilder engine();
    public abstract CarBuilder tires(Tires tires);
    public abstract CarBuilder hasCruiseControl(boolean hasCruiseControl);

    public Car build() {
       var car = new Car();

       car.setEngine(engine);
       car.setTires(tires);
       car.setHasCruiseControl(hasCruiseControl);

       return car;
    }
}
